<div class="o-modal" v-show="loginState">
    <div class="o-modal__content l-login">
        <form method="POST" action="{{ route('login') }}">
            <div class="group">
                <input type="email" class="o-input--text" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>e-mail</label>
            </div>
            <div class="group">
                <input type="password" class="o-input--text" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>password</label>
            </div>
            <button type="submit" class="o-button__primary l-login--button">Login</button>
        </form>
        <a href="{{ route('auth.social', 'facebook') }}">
            <img src="images/social_login_facebook.svg" alt="facebookでログイン" class="">
        </a>
        <a href="{{ route('auth.social', 'line') }}">
            <img src="images/social_login_line.svg" alt="LINEでログイン" class="">
        </a>
        <a href="{{ route('auth.social', 'yahoo') }}">
            <img src="images/social_login_yahoo.svg" alt="Yahooでログイン" class="">
        </a>
        <button type="button" class="o-button__primary l-login--button" v-on:click="loginState = false">新規登録</button>
        <button type="button" class="o-button__primary l-login--button" v-on:click="loginState = false">キャンセル</button>
    </div>
</div>

{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Login</div>--}}
                {{--<a href="{{ route('auth.social', 'facebook') }}" title="Facebook">--}}
                    {{--facebook<i class="fa fa-2x fa-facebook-square"></i>--}}
                {{--</a>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" method="POST" action="{{ route('login') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-8 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Login--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--Forgot Your Password?--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}
