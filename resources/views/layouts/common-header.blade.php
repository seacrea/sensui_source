<div class="l-header-container--main o-header-main">
    <a href="#"><img src="images/logo_white.svg" alt="logo" class="o-header-logo"></a>
    <div class="o-header-main-search">
        <i class="fas fa-search"></i>
        <input type="text" class="o-header-search-input" placeholder="潜水馬鹿を検索">
    </div>
    @guest
        <div id="login">
            <button class="o-header-main--link o-button--transparent" v-on:click="loginState = true">login</button>
            {{-- ログイン用モーダル --}}
            @include('auth.login')
        </div>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
               aria-haspopup="true" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>

    @endguest

</div>
<div class="l-header-container--sub o-header-sub">
    <ul class="o-header-tab">
        <li class="o-header-tabs"><a href="">詳細</a></li>
        <li class="o-header-tabs"><a href="">生物</a></li>
        <li class="o-header-tabs"><a href="">画像</a></li>
        <li class="o-header-tabs"><a href="">評価</a></li>
    </ul>
</div>
