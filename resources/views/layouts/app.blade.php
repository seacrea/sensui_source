<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/solid.css" integrity="sha384-TbilV5Lbhlwdyc4RuIV/JhD8NR+BfMrvz4BL5QFa2we1hQu6wvREr3v6XSRfCTRp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/fontawesome.css" integrity="sha384-ozJwkrqb90Oa3ZNb+yKFW2lToAWYdTiF1vt8JiH5ptTGHTGcN7qdoR1F95e0kYyG" crossorigin="anonymous">
    <title>潜水馬鹿 @yield('title')</title>
</head>
<body>
    {{-- 共通ヘッダーの読み込み --}}
    <div class="l-grid--double-header">
        <header class="l-header o-header">
            @include('layouts.common-header')
        </header>
        {{-- グローバルナビゲーションの読み込み --}}
        <nav class="l-nav">
            @include('layouts.common-nav')
        </nav>
        {{-- 各ページコンテンツの読み込み --}}
        <main class="l-main">
            @yield('content')
        </main>
    </div>
    {{-- Noto Sans Japaneseの読み込み --}}
    <script src="js/noto-fonts.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
