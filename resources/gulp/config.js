module.exports ={
    source: {
        sass: './resources/assets/sass/',
        js:   './resources/assets/js/'
    },
    dest: {
        html: './public',
        css:  './public/css/',
        js:   './public/js/'
    }
};