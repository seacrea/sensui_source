# 潜水馬鹿clone後の手順

## composer installの実施
```shell
$ composer install
```

## npm installの実施
` package.json `にインストールすべきものは記載されているので下記コマンドを実施
```shell
$ npm install
```

## .envを作成
1. ` .env.example `をコピーして ` .env `を作成する。
2. DBのパスワード等は環境に合わせて変更する。

## DBの作成
1. .envに定義したデータベースを作成する。
2. DBのマイグレーションを下記コマンドにて実施する
```shell
$ php artisan migrate
``` 

# ファイル変更時に必要な手順
## CSS変更方法
1. sass配下を変更
1. gulpを起動する。
（sassをコンパイルしてCSSに変換しないと認識されません）
```shell
# 一度だけ実施する
$ npx gulp sassc

# 監視して対象フィルが変更されたら勝手にコンパイルする
$ npx gulp wath
```

## vue.jsの変更方法
1. ` resources/assets/js `配下に` .vue `を作成する
2. laravelmixを使用してコンパイルする。
```shell
# 一度だけ実施する
$ npm run dev

# 監視して対象ファイルが変更されたら勝手にコンパイルする
$ npm run watch
```